# OpenML dataset: sarcos

https://www.openml.org/d/44882

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

Within robotics, inverse dynamics algorithms are used to calculate the torques that a robot's motors must deliver to make the robot's end-point move in the way prescribed by its current task. More about [inverse dynamics][1].

[1]: <https://en.wikipedia.org/wiki/Inverse_dynamics>

The data set consists of 45,000 data points, collected at 100Hz from the actual robot performing various rhythmic and discrete movement tasks (this corresponds to 7.5 minutes of data collection).

The task is to map from a 21-dimensional input space (7 joint positions, 7 joint velocities, 7 joint accelerations) to the corresponding 7 joint torques.

This version of the dataset includes only the training set of the original dataset, as the test dataset contains almost exclusively duplicates from the training data.

**Attribute Description**

1. *V[1-7]* - 7 joint positions
2. *V[8-14]* - 7 joint velocities
3. *V[15-21]* - 7 joint accelerations
4. *V[22-28]* - 7 joint torques, target variables, take one (*V22*) as target feature, ignore others as alternate target features

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44882) of an [OpenML dataset](https://www.openml.org/d/44882). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44882/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44882/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44882/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

